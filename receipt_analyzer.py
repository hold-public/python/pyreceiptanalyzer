import re

from lib.db_manager import store_into_db, get_spendings_for_year, get_most_purchased_items, get_most_money_spent_items
from lib.mail_reader import get_pdfs
from lib.pdf_parser import parse_pdfs

def convert_filename_coop(original_filename):
    pattern = re.compile('^.+_\d{8}_\d{4}.pdf$')
    match = pattern.match(original_filename)
    if match:
        return 'receipt_' + original_filename[-17:]
    else:
        print(f'Filename {original_filename} did not match!')
        return None

def get_data():
    # Coop
    allowed_subjects = ['Ihr digitaler Kassenzettel vom ']
    convert_filename = convert_filename_coop
    pdf_folder = 'receipts_coop'

    get_pdfs(allowed_subjects, convert_filename, pdf_folder)
    all_items_list = parse_pdfs(pdf_folder)
    store_into_db(all_items_list)

def analyze_data():
    print('Spendings per year:')
    for year in range(2017, 2021 + 1):
        spendings = get_spendings_for_year(year)
        print(f'Year {year}: {spendings}')

    print('')
    print('Most purchased items:')
    most_purchased_items = get_most_purchased_items(20)
    for rank, most_purchased_item in enumerate(most_purchased_items):
        print(f'#{rank + 1:2d}: {most_purchased_item.qty:3d}x "{most_purchased_item.name}" (total price: {most_purchased_item.price})')

    print('')
    print('Items where most money was spent on:')
    most_money_spent_items = get_most_money_spent_items(20)
    for rank, most_money_spent_item in enumerate(most_money_spent_items):
        print(f'#{rank + 1:2d}: {most_money_spent_item.qty:3d}x "{most_money_spent_item.name}" (total price: {most_money_spent_item.price})')

if __name__ == "__main__":
    get_data()
    analyze_data()
