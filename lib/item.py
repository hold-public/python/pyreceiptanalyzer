from dataclasses import dataclass
import datetime

@dataclass
class Item:
    purchase_datetime: datetime.datetime
    name: str
    qty: int
    price: float
