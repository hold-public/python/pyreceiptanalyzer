import imaplib
import email
from email.header import decode_header
import webbrowser
import os
import json
import quopri
import re
import base64

def get_pdfs(allowed_subjects, convert_filename, output_folder):
    if os.path.isdir(output_folder):
        print(f'Output folder "{output_folder}" already exists. Do you want to re-download pdfs?')
        redownload = input('y/n ')
        if (redownload.lower() == 'n'):
            return
    else:
        os.mkdir(output_folder)

    all_server_settings = get_server_settings()

    for server_settings in all_server_settings:
        print(f"Checking E-mail address {server_settings['email']}")
        get_mails(server_settings, allowed_subjects, convert_filename, output_folder)

def get_server_settings():
    JSON_FILE_NAME = 'server_settings.json'
    if os.path.isfile(JSON_FILE_NAME):
        with open(JSON_FILE_NAME, 'r') as fp:
            server_settings = json.load(fp)
        print(f'Opening settings from file {JSON_FILE_NAME}.')
    else:
        server_settings = []
        while True:
            print('Please enter server settings.')
            imap_server = input('Ingoing IMAP server address: ')
            folder = input('IMAP folder where the receipts are: ')
            email = input('E-Mail: ')
            password = input('Password: ')
            credentials = {
                'imap_server': imap_server,
                'folder': folder,
                'email': email,
                'password': password,
            }

            server_settings.append(credentials)
            print('Would you like to enter another server?')
            another = input('y/n: ')
            if (another.lower() == 'n'):
                break
        with open(JSON_FILE_NAME, 'w') as fp:
            json.dump(server_settings, fp, indent=4)
        print(f'To later edit these settings, either manually edit the file {JSON_FILE_NAME}, or delete it to re-enter settings.')
    return server_settings

def get_mails(server_settings, allowed_subjects, convert_filename, output_folder):
    # create an IMAP4 class with SSL
    imap = imaplib.IMAP4_SSL(server_settings['imap_server'])
    # authenticate
    imap.login(server_settings['email'], server_settings['password'])

    status, messages = imap.select(server_settings['folder'], readonly=True)

    # total number of emails
    messages = int(messages[0])

    for i in range(messages, 0, -1):
        # fetch the email message by ID
        res, msg = imap.fetch(str(i), "(RFC822)")
        for response in msg:
            if isinstance(response, tuple):
                # parse a bytes email into a message object
                msg = email.message_from_bytes(response[1])

                # decode the email subject
                subject, encoding = decode_header(msg["Subject"])[0]
                if encoding is None:
                    encoding = 'utf-8'
                if encoding == 'unknown-8bit':
                    print(f'unknown-8bit (subject: {subject})')
                    continue
                if isinstance(subject, bytes):
                    # if it's a bytes, decode to str
                    subject = subject.decode(encoding)
                subject_partial_match = any([allowed_subject in subject for allowed_subject in allowed_subjects])

                if subject_partial_match:
                    # if the email message is multipart
                    if msg.is_multipart():
                        # iterate over email parts
                        for part in msg.walk():
                            # extract content type of email
                            content_disposition = str(part.get("Content-Disposition"))

                            if "attachment" in content_disposition:
                                # download attachment
                                filename_raw = part.get_filename()
                                filename_decoded = decode_filename(filename_raw)
                                filename = convert_filename(filename_decoded)
                                if filename:
                                    filepath = os.path.join(output_folder, filename)
                                    # download attachment and save it
                                    open(filepath, "wb").write(part.get_payload(decode=True))

    # close the connection and logout
    imap.close()
    imap.logout()

def decode_filename(filename_raw):
    try:
        encoded_word_regex = r'=\?{1}(.+)\?{1}([B|Q])\?{1}(.+)\?{1}='
        charset, encoding, encoded_text = re.match(encoded_word_regex, filename_raw).groups()
        if encoding == 'B':
            byte_string = base64.b64decode(encoded_text)
        elif encoding == 'Q':
            byte_string = quopri.decodestring(encoded_text)
        return byte_string.decode(charset)
    except:
        return filename_raw
