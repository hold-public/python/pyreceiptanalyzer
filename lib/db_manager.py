import datetime
import psycopg2

from lib.item import Item

ITEM_TABLE_NAME = 'items'

class Connection(object):
    def __init__(self):
        pass

    def __enter__(self):
        self.conn = login()
        return self.conn

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(f'exc_type: {exc_type}')
            print(f'exc_value: {exc_value}')
            print(f'exc_traceback: {exc_traceback}')

        if self.conn is not None:
            self.conn.close()


def store_into_db(list_of_items):
    with Connection() as conn:
        create_item_table(conn)

        for item in list_of_items:
            insert_item(conn, item)

def login():
    conn = psycopg2.connect(
        host='localhost',
        database='hello_world_db',
        user='postgres',
        password='abcd'
    )
    return conn

def create_item_table(conn):
    with conn.cursor() as cur:
        cur.execute(f"""
        DROP TABLE IF EXISTS {ITEM_TABLE_NAME};
        """)

        cur.execute(f"""
        CREATE TABLE IF NOT EXISTS {ITEM_TABLE_NAME} (
            purchase_datetime TIMESTAMP,
            name VARCHAR(255) NOT NULL,
            qty INTEGER NOT NULL,
            price MONEY NOT NULL
        )
        """)

        conn.commit()

def insert_item(conn, item):
    with conn.cursor() as cur:
        command_insert = f"""
        INSERT INTO {ITEM_TABLE_NAME}
        VALUES(%s, %s, %s, %s);
        """

        dt = datetime.datetime.now()

        cur.execute(command_insert, (item.purchase_datetime, item.name, item.qty, item.price))

        conn.commit()

def get_spendings_for_year(year):
    with Connection() as conn:
        with conn.cursor() as cur:
            cur.execute(f"""
            SELECT SUM(price) FROM {ITEM_TABLE_NAME}
            WHERE EXTRACT('year' from purchase_datetime) = {year};
            """)

            result_rows = cur.fetchone()
            return result_rows[0]

def get_most_purchased_items(number_of_items):
    with Connection() as conn:
        with conn.cursor() as cur:
            cur.execute(f"""
            SELECT name, sum(qty), sum(price) FROM {ITEM_TABLE_NAME}
            GROUP BY name
            ORDER BY sum(qty) DESC;
            """)

            most_purchased_raw_items = cur.fetchmany(number_of_items)
            most_purchased_items = []
            for most_purchased_raw_item in most_purchased_raw_items:
                most_purchased_items.append(Item(
                    purchase_datetime = 0,
                    name = most_purchased_raw_item[0],
                    qty = most_purchased_raw_item[1],
                    price = most_purchased_raw_item[2],
                ))
    return most_purchased_items

def get_most_money_spent_items(number_of_items):
    with Connection() as conn:
        with conn.cursor() as cur:
            cur.execute(f"""
            SELECT name, sum(qty), sum(price) FROM {ITEM_TABLE_NAME}
            GROUP BY name
            ORDER BY sum(price) DESC;
            """)

            most_money_spent_raw_items = cur.fetchmany(number_of_items)
            most_money_spent_items = []
            for most_money_spent_raw_item in most_money_spent_raw_items:
                most_money_spent_items.append(Item(
                    purchase_datetime = 0,
                    name = most_money_spent_raw_item[0],
                    qty = most_money_spent_raw_item[1],
                    price = most_money_spent_raw_item[2],
                ))
    return most_money_spent_items
