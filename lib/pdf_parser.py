import datetime
import os
import pandas as pd
from tabula import read_pdf

from lib.item import Item

class NoHeaderException(Exception):
    pass

class TableNotFoundException(Exception):
    pass

def parse_pdfs(pdf_folder):
    all_items = []
    for filename in os.listdir(pdf_folder):
        if filename.endswith(".pdf"):
            pdf_file_path = os.path.join(pdf_folder, filename)
            try:
                purchase_datetime = filename_2_datetime(filename)
                new_items = parse_pdf(purchase_datetime, pdf_file_path)
                all_items += new_items
            except TableNotFoundException:
                print(f'File {filename} has no detectable table!')
            except NoHeaderException:
                print(f'File {filename} has no header!')
            except BaseException as be:
                print(f'File {filename} has unknown error ({be})')
    return all_items

def filename_2_datetime(filename):
    datetime_str = filename.lstrip('receipt_').rstrip('.pdf')

    return datetime.datetime.strptime(datetime_str, '%Y%m%d_%H%M')

def parse_pdf(purchase_datetime, pdf_file_path):
    list_of_tables = read_pdf(pdf_file_path, pages="all")

    if not list_of_tables:
        raise TableNotFoundException

    table_rows = list_of_tables[0]
    translate(table_rows)

    if 'Artikel' not in table_rows.columns:
        raise NoHeaderException

    table_rows['Menge'] = pd.to_numeric(table_rows['Menge'], errors='coerce')
    table_rows['Preis'] = pd.to_numeric(table_rows['Preis'], errors='coerce')
    table_rows['Aktion'] = pd.to_numeric(table_rows['Aktion'], errors='coerce')
    table_rows['Total'] = pd.to_numeric(table_rows['Total'], errors='coerce')

    row_has_items = table_rows['Zusatz'].notna()
    item_rows = table_rows[row_has_items]

    items = []
    for _, item_row in item_rows.iterrows():
        item = Item(
            purchase_datetime = purchase_datetime,
            name = item_row.Artikel,
            qty = item_row.Menge,
            price = item_row.Preis,
        )
        items.append(item)

    return items

def translate(df):
    if 'Article' in df.columns:
        # French!
        df.rename(columns = {
            'Article': 'Artikel',
            'Quantité': 'Menge',
            'Prix': 'Preis',
            'Action': 'Aktion',
            'Total': 'Total',
            'Infos': 'Zusatz',
        }, inplace=True)
